from PyQt5.Qt import Qt
from PyQt5.QtWidgets import QWidget

from UI_FavouritesPopup import Ui_FavouritesPopup


class FavouritesPopup(QWidget):
    def __init__(self, data_store, image, rover, camera, rotation, user_id):
        super().__init__()
        self.ui = Ui_FavouritesPopup()
        self.ui.setupUi(self)
        self.data_store = data_store

        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.buttons = [self.ui.CancelButton, self.ui.AddFavouritesButton]

        for button in self.buttons:
            button.setFocusPolicy(Qt.NoFocus)

        self.setup_events()

        self.image = image
        self.rover = rover
        self.camera = camera
        self.rotation = rotation
        self.user_id = user_id

    def setup_events(self):
        self.ui.CancelButton.clicked.connect(self.cancel)
        self.ui.AddFavouritesButton.clicked.connect(self.add_to_favourites)

    def cancel(self):
        self.close()

    def add_to_favourites(self):
        try:
            if len(self.ui.NameLineEdit.text()) > 0:
                name = self.ui.NameLineEdit.text()

                if len(self.ui.TagLineLineEdit.text()) > 0:
                    tag_line = self.ui.TagLineLineEdit.text()
                else:
                    tag_line = "general"

                if name not in self.data_store.get_favourites(tag_line, self.user_id):
                    self.image.save(f"Images/{name}.jpg", "JPEG")
                    self.data_store.save_image_as_favourite(name, self.rover, self.camera, self.rotation,
                                                            tag_line, self.user_id)

                    self.close()
                else:
                    print("Name already in use!")
            else:
                print("Please enter a name for the image!")
        except Exception as e:
            print(str(e))
