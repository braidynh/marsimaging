from PyQt5.Qt import Qt
from PyQt5.QtWidgets import QWidget

from SuccessPopup import SuccessPopup
from UI_ImagingPropertiesPopup import Ui_ImagingPropertiesPopup


class ImagingPropertiesPopup(QWidget):
    def __init__(self, main, data_store, api_key, favourites_location):
        super().__init__()
        self.ui = Ui_ImagingPropertiesPopup()
        self.ui.setupUi(self)
        self.main = main
        self.data_store = data_store

        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.buttons = [self.ui.CancelButton, self.ui.UpdateButton]

        for button in self.buttons:
            button.setFocusPolicy(Qt.NoFocus)

        self.ui.APIKeyLineEdit.setText(api_key)
        self.ui.FavouritesLocationLineEdit.setText(favourites_location)

        self.setup_events()

    def setup_events(self):
        self.ui.CancelButton.clicked.connect(self.cancel)
        self.ui.UpdateButton.clicked.connect(self.update_settings)

    def cancel(self):
        self.close()

    def update_settings(self):
        try:
            file = open("Settings.py", "r")
            file_lines = file.readlines()
            file_lines[4] = f"api_key = '{self.ui.APIKeyLineEdit.text()}'\n"
            file_lines[5] = f"favourites_location = '{self.ui.FavouritesLocationLineEdit.text()}'\n"
            file.close()

            file = open("Settings.py", "w")
            file.writelines(file_lines)

            self.popup = SuccessPopup("Success! Please restart.")
            self.popup.show()
        except Exception as e:
            print(str(e))

