from PyQt5.Qt import Qt
from PyQt5.QtWidgets import QWidget

from UI_EditProxyPopup import Ui_EditProxyPopup

from SuccessPopup import SuccessPopup


class ProxySettingsPopup(QWidget):
    def __init__(self, main, data_store, domain, username, password, user_agent):
        super().__init__()
        self.ui = Ui_EditProxyPopup()
        self.ui.setupUi(self)
        self.main = main
        self.data_store = data_store

        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.buttons = [self.ui.CancelButton, self.ui.UpdateButton]

        for button in self.buttons:
            button.setFocusPolicy(Qt.NoFocus)

        self.ui.DomainLineEdit.setText(domain)
        self.ui.UsernameLineEdit.setText(username)
        self.ui.PasswordLineEdit.setText(password)
        self.ui.UserAgentLineEdit.setText(user_agent)

        self.setup_events()

    def setup_events(self):
        self.ui.CancelButton.clicked.connect(self.cancel)
        self.ui.UpdateButton.clicked.connect(self.update_settings)

    def cancel(self):
        self.close()

    def update_settings(self):
        try:
            file = open("Settings.py", "r")
            file_lines = file.readlines()
            file_lines[0] = f"proxy_domain = '{self.ui.DomainLineEdit.text()}'\n"
            file_lines[2] = f"proxy_username = '{self.ui.UsernameLineEdit.text()}'\n"
            file_lines[3] = f"proxy_password = '{self.ui.PasswordLineEdit.text()}'\n"
            file_lines[4] = f"user_agent = '{self.ui.UserAgentLineEdit.text()}'\n"
            file.close()

            file = open("Settings.py", "w")
            file.writelines(file_lines)

            self.popup = SuccessPopup("Success! Please restart.")
            self.popup.show()
        except Exception as e:
            print(str(e))
