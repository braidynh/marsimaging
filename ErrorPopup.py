from PyQt5.Qt import Qt
from PyQt5.QtWidgets import QWidget

from UI_ErrorPopup import Ui_ErrorPopup


class ErrorPopup(QWidget):
    def __init__(self, error):
        super().__init__()
        self.ui = Ui_ErrorPopup()
        self.ui.setupUi(self)

        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.ui.OkButton.setFocusPolicy(Qt.NoFocus)

        self.setup_events()

        self.ui.ErrorLabel.setText(error)

    def setup_events(self):
        self.ui.OkButton.clicked.connect(self.cancel)

    def cancel(self):
        self.close()

