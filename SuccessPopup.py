from PyQt5.Qt import Qt
from PyQt5.QtWidgets import QWidget

from UI_SuccessPopup import Ui_SuccessPopup


class SuccessPopup(QWidget):
    def __init__(self, message):
        super().__init__()
        self.ui = Ui_SuccessPopup()
        self.ui.setupUi(self)

        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.ui.OkButton.setFocusPolicy(Qt.NoFocus)

        self.setup_events()

        self.ui.SuccessLabel.setText(message)

    def setup_events(self):
        self.ui.OkButton.clicked.connect(self.cancel)

    def cancel(self):
        self.close()

