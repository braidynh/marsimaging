import subprocess

import requests

from Settings import *


class DataStore:
    def __init__(self):
        self.headers = {"User-Agent": user_agent}
        self.proxies = {"http": f"http://{proxy_username}:{proxy_password}@{proxy_domain}:{str(proxy_port)}",
                        "https": f"http://{proxy_username}:{proxy_password}@{proxy_domain}:{str(proxy_port)}"}

    @staticmethod
    def get_request(func, return_list, args=None):
        first_item = True
        if args is None:
            result = requests.get(f"http://127.0.0.1:8000/api/{func}")
        else:
            request_url = f"http://127.0.0.1:8000/api/{func}"
            for arg1, arg2 in args.items():
                if first_item:
                    request_url += f"?{arg1}={arg2}"
                    first_item = False
                else:
                    request_url += f"&{arg1}={arg2}"

            result = requests.get(request_url)

        func_string = func.split("_")
        if func_string[0] == "select" or func_string[0] == "check" or func_string[0] == "get":
            result_list = []
            result_data = result.json()["results"]

            if not return_list:
                return result_data
            else:
                for index, list in enumerate(result_data):
                    for key, value in result_data[index].items():
                        result_list.append(value)

            return result_list
        else:
            print("Operation Successful")

    def check_user_details(self, username, password):
        user_details = self.get_request("get_user_details", True, {"username": username})
        if len(user_details) > 0:
            if username == user_details[1] and password == user_details[2]:
                return True
            else:
                return False

    def get_user_details(self, username):
        user_details = self.get_request("get_user_details", True, {"username": username})
        return user_details[0], user_details[3], user_details[4]

    def get_all_images(self, rover, sol, camera):
        image = requests.get(
            f"https://api.nasa.gov/mars-photos/api/v1/rovers/{rover}/photos?sol={sol}&camera={camera}&api_key={api_key}",
            headers=self.headers,
            proxies=self.proxies)
        images = image.json()["photos"]

        image_list = []
        for photo in images:
            image_value_list = {"id": photo["id"], "img_src": photo["img_src"]}
            image_list.append(image_value_list)

        return image_list

    def get_image(self, url):
        image = requests.get(url, headers=self.headers, proxies=self.proxies)
        return image

    def save_image_as_favourite(self, name, rover, camera, rotation, tag, user_id):
        try:
            self.get_request("insert_favourite", False, {"name": name, "rover": rover, "camera": camera,
                                                         "rotation": rotation, "tag": tag, "user_id": user_id})
        except Exception as e:
            print(str(e))

    def register_new_user(self, username, password, given_names, last_name):
        self.get_request("register_user", True, {"username": username, "password": password, "given_names": given_names,
                                                 "last_name": last_name})

    def get_favourites(self, tag, user_id):
        favourites = self.get_request("get_favourites", True, {"tag": tag, "user_id": user_id})
        return favourites

    def get_favourite_tags(self):
        tags = self.get_request("get_favourite_tags", True)
        return tags

    def get_favourite_image(self, tag, user_id, name):
        image = self.get_request("get_favourite_image", True, {"tag": tag, "user_id": user_id, "name": name})
        return image

    def remove_favourite_image(self, tag, user_id, name):
        self.get_request("remove_from_favourites", True, {"tag": tag, "user_id": user_id, "name": name})
