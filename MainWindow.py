import os
import sys

from PyQt5.QtCore import Qt
from PyQt5.QtGui import QPixmap, QImage
from PyQt5.QtWidgets import QWidget, QApplication

from DataStore import DataStore
from ErrorPopup import ErrorPopup
from FavouritesPopup import FavouritesPopup
from ProxySettingsPopup import ProxySettingsPopup
from ImagingPropertiesPopup import ImagingPropertiesPopup
from UI_MainWindow import Ui_MainWindow

from Settings import *


class MainWindow(QWidget):
    def __init__(self):
        super().__init__()
        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self.data_store = DataStore()

        self.drag_move = False
        self.drag_position = 0

        self.setWindowFlag(Qt.FramelessWindowHint)
        self.setAttribute(Qt.WA_TranslucentBackground)

        self.setup_events()

        self.disabled_widgets = [self.ui.DashboardButton, self.ui.SearchButton, self.ui.SearchBarLineEdit]
        self.hidden_widgets = [self.ui.AccountLogoLabel, self.ui.SearchBarLineEdit, self.ui.SearchButton]
        self.disabled_buttons = [self.ui.ImagingButton, self.ui.SettingsButton, self.ui.AboutButton,
                                 self.ui.LogoutButton]

        self.cleared_widgets = [self.ui.UsernameLineEdit, self.ui.PasswordLineEdit, self.ui.UsernameRegisterLineEdit,
                                self.ui.PasswordRegisterLineEdit, self.ui.GivenNamesLineEdit, self.ui.LastNameLineEdit]

        self.buttons = [self.ui.ViewFavouritesButton, self.ui.AddFavouritesButton, self.ui.ReturnImagingButton,
                        self.ui.RemoveFavouritesButton, self.ui.RegisterButton, self.ui.LoginButton,
                        self.ui.EditProxySettingsButton, self.ui.EditImagingButton]

        for button in self.buttons:
            button.setFocusPolicy(Qt.NoFocus)

        for widget in self.disabled_widgets:
            widget.setDisabled(True)

        for button in self.disabled_buttons:
            button.setDisabled(True)

        for widget in self.hidden_widgets:
            widget.setVisible(False)

        self.selected_button = None

        self.curiosity_cameras = ["FHAZ", "RHAZ", "MAST", "CHEMCAM", "MAHLI", "MARDI", "NAVCAM"]
        self.opportunity_cameras = ["FHAZ", "RHAZ", "NAVCAM", "PANCAM", "MINITES"]
        self.spirit_cameras = ["FHAZ", "RHAZ", "NAVCAM", "PANCAM", "MINITES"]

        self.image_list = []

        self.old_sol_value = 0

        self.user_id = 1

        self.setup_widgets()

    def mousePressEvent(self, event):
        if event.buttons() == Qt.LeftButton and event.y() < 50:
            self.drag_move = True
            self.drag_position = event.globalPos() - self.pos()
            event.accept()

    def mouseMoveEvent(self, event):
        try:
            if self.drag_move:
                self.move(event.globalPos() - self.drag_position)
                event.accept()
        except AttributeError:
            pass

    def mouseReleaseEvent(self, event):
        self.drag_move = False

    def close(self):
        sys.exit(app.exec_())

    def setup_widgets(self):
        self.update_tags()
        self.update_proxy_settings()
        self.update_imaging_properties()

    def setup_events(self):
        self.ui.DashboardButton.clicked.connect(self.switch_stacked_widget)
        self.ui.ImagingButton.clicked.connect(self.switch_stacked_widget)
        self.ui.SettingsButton.clicked.connect(self.switch_stacked_widget)
        self.ui.AboutButton.clicked.connect(self.switch_stacked_widget)
        self.ui.ViewFavouritesButton.clicked.connect(self.switch_stacked_widget)
        self.ui.ReturnImagingButton.clicked.connect(self.switch_stacked_widget)

        self.ui.QuitButton.clicked.connect(self.close)

        self.ui.RoverComboBox.currentIndexChanged.connect(self.switch_rover)
        self.ui.SolSpinBox.editingFinished.connect(self.get_photos)
        self.ui.CameraComboBox.currentIndexChanged.connect(self.get_photos)

        self.ui.PhotoListWidget.itemClicked.connect(self.get_photo)

        self.ui.AddFavouritesButton.clicked.connect(self.add_to_favourites)

        self.ui.LoginButton.clicked.connect(self.login)
        self.ui.LogoutButton.clicked.connect(self.logout)
        self.ui.RegisterButton.clicked.connect(self.register)

        self.ui.TagComboBox.currentIndexChanged.connect(self.update_favourites)
        self.ui.FavouritesListWidget.itemClicked.connect(self.get_favourite_image)

        self.ui.RemoveFavouritesButton.clicked.connect(self.remove_image_from_favourites)

        self.ui.EditProxySettingsButton.clicked.connect(self.edit_proxy_settings)
        self.ui.EditImagingButton.clicked.connect(self.edit_imaging_properties)

    def login(self):
        if len(self.ui.UsernameLineEdit.text()) > 0 and len(self.ui.PasswordLineEdit.text()) > 0:
            if self.data_store.check_user_details(self.ui.UsernameLineEdit.text(), self.ui.PasswordLineEdit.text()):
                self.selected_button = self.ui.DashboardButton
                self.ui.DashboardButton.setDisabled(False)
                self.ui.DashboardButton.setFocusPolicy(Qt.NoFocus)
                self.ui.DashboardButton.setStyleSheet("""
                QPushButton
                {
                    background-color: rgb(29, 28, 37);
                    color: white;
                    border-radius: 20px;
                    outline: 0;
                }
    
                QPushButton:hover
                {
                    background-color: rgb(29, 28, 37);
                }
                """)

                for widget in self.disabled_widgets:
                    widget.setDisabled(False)

                for widget in self.hidden_widgets:
                    widget.setVisible(True)

                for button in self.disabled_buttons:
                    button.setDisabled(False)
                    button.setFocusPolicy(Qt.NoFocus)
                    button.setStyleSheet("""
                    QPushButton
                    {
                        background-color: transparent;
                        color: white;
                        border-radius: 20px;
                        outline: 0;
                    }
    
                    QPushButton:hover
                    {
                        background-color: rgb(29, 28, 37)
                    }
                    """)

                self.ui.SearchButton.setStyleSheet("""
                QPushButton
                {
                    background-color: transparent;
                    color: white;
                    border: none;
                    outline: 0;
                }
                """)
                self.ui.SearchButton.setFocusPolicy(Qt.NoFocus)

                user_details = self.data_store.get_user_details(self.ui.UsernameLineEdit.text())
                self.ui.AccountLabel.setText(f"{user_details[2].upper()}, {user_details[1].upper()}")
                self.user_id = user_details[0]

                for widget in self.cleared_widgets:
                    widget.setText("")

                self.setup_widgets()
                self.ui.StackedWidget.setCurrentWidget(self.ui.DashboardPage)
            else:
                self.popup = ErrorPopup("Incorrect login details!")
                self.popup.show()
        else:
            self.popup = ErrorPopup("Please enter a username and password!")
            self.popup.show()

    def logout(self):
        self.ui.StackedWidget.setCurrentWidget(self.ui.LoginPage)

        for widget in self.disabled_widgets:
            widget.setDisabled(True)

        for button in self.disabled_buttons:
            button.setDisabled(True)
            button.setStyleSheet("""
            QPushButton
            {
                background-color: transparent;
                color: grey;
                border-radius: 20px;
                outline: 0;
            }
            """)

        self.ui.DashboardButton.setStyleSheet("""
        QPushButton
        {
            background-color: transparent;
            color: grey;
            border-radius: 20px;
            outline: 0;
        }
        """)

        self.ui.SearchButton.setStyleSheet("""
        QPushButton
        {
            background-color: transparent;
            color: grey;
            border: none;
            outline: 0;
        }
        """)

        for widget in self.hidden_widgets:
            widget.setVisible(False)

        self.selected_button = None

        self.ui.AccountLabel.setText("")
        self.user_id = 0

        self.ui.ImagingLabel.clear()
        self.ui.RoverComboBox.setCurrentIndex(0)
        self.ui.CameraComboBox.clear()
        for camera in self.curiosity_cameras:
            self.ui.CameraComboBox.addItem(camera)
        self.ui.CameraComboBox.setCurrentIndex(0)
        self.ui.SolSpinBox.setValue(0)
        self.ui.PhotoListWidget.clear()

        self.ui.FavouritesLabel.clear()
        self.ui.RoverFavouritesLabel.setText("Rover:")
        self.ui.CameraFavouritesLabel.setText("Camera:")
        self.ui.SolFavouritesLabel.setText("Sol:")

    def register(self):
        if len(self.ui.UsernameRegisterLineEdit.text()) > 0 and len(self.ui.PasswordRegisterLineEdit.text()) > 0 \
                and len(self.ui.GivenNamesLineEdit.text()) > 0 and len(self.ui.LastNameLineEdit.text()) > 0:
            self.data_store.register_new_user(self.ui.UsernameRegisterLineEdit.text(),
                                              self.ui.PasswordRegisterLineEdit.text(),
                                              self.ui.GivenNamesLineEdit.text().lower(),
                                              self.ui.LastNameLineEdit.text().lower())

            self.selected_button = self.ui.DashboardButton
            self.ui.DashboardButton.setDisabled(False)
            self.ui.DashboardButton.setFocusPolicy(Qt.NoFocus)
            self.ui.DashboardButton.setStyleSheet("""
                            QPushButton
                            {
                                background-color: rgb(29, 28, 37);
                                color: white;
                                border-radius: 20px;
                                outline: 0;
                            }

                            QPushButton:hover
                            {
                                background-color: rgb(29, 28, 37);
                            }
                            """)

            for widget in self.disabled_widgets:
                widget.setDisabled(False)

            for widget in self.hidden_widgets:
                widget.setVisible(True)

            for button in self.disabled_buttons:
                button.setDisabled(False)
                button.setFocusPolicy(Qt.NoFocus)
                button.setStyleSheet("""
                                QPushButton
                                {
                                    background-color: transparent;
                                    color: white;
                                    border-radius: 20px;
                                    outline: 0;
                                }

                                QPushButton:hover
                                {
                                    background-color: rgb(29, 28, 37)
                                }
                                """)

            self.ui.SearchButton.setStyleSheet("""
                            QPushButton
                            {
                                background-color: transparent;
                                color: white;
                                border: none;
                                outline: 0;
                            }
                            """)
            self.ui.SearchButton.setFocusPolicy(Qt.NoFocus)

            self.ui.AccountLabel.setText(
                f"{self.ui.LastNameLineEdit.text().upper()}, {self.ui.GivenNamesLineEdit.text().upper()}")
            user_details = self.data_store.get_user_details(self.ui.UsernameRegisterLineEdit.text())
            self.user_id = user_details[0]

            for widget in self.cleared_widgets:
                widget.setText("")

            self.ui.StackedWidget.setCurrentWidget(self.ui.DashboardPage)
        else:
            self.popup = ErrorPopup("Please enter your details into the given fields!")
            self.popup.show()

    def switch_stacked_widget(self):
        button = self.sender().objectName()

        if button == "DashboardButton":
            widget = self.ui.DashboardPage
            button = self.ui.DashboardButton
        elif button == "ImagingButton":
            widget = self.ui.ImagingPage
            button = self.ui.ImagingButton
        elif button == "SettingsButton":
            widget = self.ui.SettingsPage
            button = self.ui.SettingsButton
        elif button == "AboutButton":
            widget = self.ui.AboutPage
            button = self.ui.AboutButton
        elif button == "ViewFavouritesButton":
            widget = self.ui.FavouritesPage
            button = self.ui.ImagingButton

            self.update_favourites()
        else:
            widget = self.ui.ImagingPage
            button = self.ui.ImagingButton

        self.ui.StackedWidget.setCurrentWidget(widget)
        self.selected_button.setStyleSheet("""
        QPushButton
        {
            background-color: transparent;
            color: white;
            border-radius: 20px;
        }

        QPushButton:hover
        {
            background-color: rgb(29, 28, 37);
        }
        """)
        button.setStyleSheet("""
        QPushButton
        {
            background-color: rgb(29, 28, 37);
            color: white;
            border-radius: 20px;
        }

        QPushButton:hover
        {
            background-color: rgb(29, 28, 37);
        }
        """)

        self.selected_button = button

    def switch_rover(self):
        if self.ui.RoverComboBox.currentText() == "Curiosity":
            cameras = self.curiosity_cameras
        elif self.ui.RoverComboBox.currentText() == "Opportunity":
            cameras = self.opportunity_cameras
        else:
            cameras = self.spirit_cameras

        self.ui.CameraComboBox.clear()
        for camera in cameras:
            self.ui.CameraComboBox.addItem(camera)
        self.ui.CameraComboBox.setCurrentIndex(0)

        self.get_photos()

    def update_list_widget(self, image_list):
        self.ui.PhotoListWidget.clear()

        for photo in image_list:
            self.ui.PhotoListWidget.addItem(str(photo["id"]))

    def update_tags(self):
        items = [self.ui.TagComboBox.itemText(i) for i in range(self.ui.TagComboBox.count())]
        tags = self.data_store.get_favourite_tags()
        single_tags = list(dict.fromkeys(tags))
        for tag in single_tags:
            if tag not in items:
                self.ui.TagComboBox.addItem(tag)

    def get_photos(self):
        sender = self.sender().objectName()
        if sender == "SolSpinBox":
            if self.ui.SolSpinBox.value() != self.old_sol_value:
                try:
                    image_list = self.data_store.get_all_images(self.ui.RoverComboBox.currentText().lower(),
                                                                self.ui.SolSpinBox.value(),
                                                                self.ui.CameraComboBox.currentText().lower())

                    self.update_list_widget(image_list)
                    self.image_list = image_list
                except Exception as e:
                    self.popup = ErrorPopup(str(e))
                    self.popup.show()

                self.old_sol_value = self.ui.SolSpinBox.value()
        else:
            try:
                image_list = self.data_store.get_all_images(self.ui.RoverComboBox.currentText().lower(),
                                                            self.ui.SolSpinBox.value(),
                                                            self.ui.CameraComboBox.currentText().lower())

                self.update_list_widget(image_list)
                self.image_list = image_list
            except Exception as e:
                self.popup = ErrorPopup(str(e))
                self.popup.show()

    def get_photo(self):
        try:
            for photo in self.image_list:
                if photo["id"] == int(self.ui.PhotoListWidget.currentItem().text()):
                    url = photo["img_src"]

            requested_image = self.data_store.get_image(url)
            display_image = QImage()
            display_image.loadFromData(requested_image.content)

            self.ui.ImagingLabel.setPixmap(QPixmap(display_image))
            self.ui.ImagingLabel.setAlignment(Qt.AlignCenter)
        except Exception as e:
            self.popup = ErrorPopup(str(e))
            self.popup.show()

    def add_to_favourites(self):
        if len(self.ui.PhotoListWidget.selectedItems()) != 0:
            image = self.ui.ImagingLabel.pixmap().toImage()
            rover = self.ui.RoverComboBox.currentText()
            camera = self.ui.CameraComboBox.currentText()
            rotation = self.ui.SolSpinBox.value()

            self.popup = FavouritesPopup(self.data_store, image, rover, camera, rotation, self.user_id)
            self.popup.show()
        else:
            self.popup = ErrorPopup("Please select an image!")
            self.popup.show()

    def update_favourites(self):
        self.update_tags()
        self.ui.FavouritesListWidget.clear()
        favourites = self.data_store.get_favourites(self.ui.TagComboBox.currentText(), self.user_id)
        for favourite in favourites:
            self.ui.FavouritesListWidget.addItem(str(favourite))

    def get_favourite_image(self):
        image_details = self.data_store.get_favourite_image(self.ui.TagComboBox.currentText(), self.user_id,
                                                            self.ui.FavouritesListWidget.currentItem().text())
        image_name = image_details[0]
        rover = image_details[1]
        camera = image_details[2]
        sol = image_details[3]

        with open(f"Images/{image_name}.jpg", "rb") as f:
            content = f.read()

        display_image = QImage()
        display_image.loadFromData(content)

        self.ui.FavouritesLabel.setPixmap(QPixmap(display_image))
        self.ui.FavouritesLabel.setAlignment(Qt.AlignCenter)

        self.ui.RoverFavouritesLabel.setText(f"Rover: {rover}")
        self.ui.CameraFavouritesLabel.setText(f"Camera: {camera}")
        self.ui.SolFavouritesLabel.setText(f"Sol: {str(sol)}")

    def remove_image_from_favourites(self):
        if len(self.ui.FavouritesListWidget.selectedItems()) != 0:
            name = self.ui.FavouritesListWidget.currentItem().text()
            self.data_store.remove_favourite_image(self.ui.TagComboBox.currentText(), self.user_id, name)
            os.remove(f"Images/{name}.jpg")

            self.update_favourites()
            self.ui.FavouritesLabel.clear()
            self.ui.RoverFavouritesLabel.setText("Rover:")
            self.ui.CameraFavouritesLabel.setText("Camera:")
            self.ui.SolFavouritesLabel.setText("Sol:")
        else:
            self.popup = ErrorPopup("Please select an image to remove!")
            self.popup.show()

    def edit_proxy_settings(self):
        self.popup = ProxySettingsPopup(self, self.data_store, proxy_domain, proxy_username, proxy_password, user_agent)
        self.popup.show()

    def edit_imaging_properties(self):
        self.popup = ImagingPropertiesPopup(self, self.data_store, api_key, favourites_location)
        self.popup.show()

    def update_proxy_settings(self):
        self.ui.CurrentProxyDomainLabel.setText(f"Domain: {proxy_domain}")
        self.ui.CurrentProxyUsernameLabel.setText(f"Username: {proxy_username}")
        self.ui.CurrentProxyUserAgentLabel.setText(f"User-Agent: {user_agent}")
        password_list = list(proxy_password)
        password = str(password_list[0])
        for i in range(len(password_list) - 3):
            password += "•"
        password += password_list[len(password_list) - 1]
        password += password_list[len(password_list) - 2]

        self.ui.CurrentProxyPasswordLabel.setText(f"Password: {password}")

    def update_imaging_properties(self):
        self.ui.ImagingPropertiesLocationLabel.setText(f"Favourites Location: {favourites_location}")
        self.ui.ImagingPropertiesAPIKeyLabel.setText(f"API Key: {api_key}")


if __name__ == "__main__":
    app = QApplication(sys.argv)
    main_window = MainWindow()
    main_window.show()
    app.exec_()
